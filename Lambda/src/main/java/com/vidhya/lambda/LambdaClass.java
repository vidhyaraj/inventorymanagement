package com.vidhya.lambda;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.SQSEvent;
import com.amazonaws.services.lambda.runtime.events.SQSEvent.SQSMessage;
import com.fasterxml.jackson.databind.ObjectMapper;

@SuppressWarnings("deprecation")
public class LambdaClass implements RequestHandler<SQSEvent, String> {
	Map<String, Request> requestMap = new HashMap<String, Request>();
	
	public class Request {
		public String type;
		public String url;
		public Request(String type , String url) {
			this.type = type;
			this.url = url;
		}
	}
	
	public LambdaClass() {
		requestMap.put("createSupplier", new Request("POST","http://jarpurchasenew-env.eba-f9ryptz5.us-east-2.elasticbeanstalk.com/inventoryManagement/purchase/api/supplier/create"));
		requestMap.put("addStock", new Request("POST","http://jarpurchasenew-env.eba-f9ryptz5.us-east-2.elasticbeanstalk.com/inventoryManagement/purchase/api/stocks/adds"));
		requestMap.put("getSuppliers", new Request("GET","http://jarpurchasenew-env.eba-f9ryptz5.us-east-2.elasticbeanstalk.com/inventoryManagement/purchase/api/suppliers"));
		requestMap.put("getStocks", new Request("GET","http://jarpurchasenew-env.eba-f9ryptz5.us-east-2.elasticbeanstalk.com/inventoryManagement/purchase/api/stocks"));
	}
	
	@SuppressWarnings("resource")
	private String httpSendAndRecv(HttpUriRequest request) {
		HttpClient client = new DefaultHttpClient();
		StringBuilder retval = new StringBuilder("");
		HttpResponse response;
		try {
			response = client.execute(request);
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			String line = "";
			while ((line = rd.readLine()) != null) {
				retval.append(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		client.getConnectionManager().shutdown();
		if (retval.length() != 0) {
			return retval.toString();
		}
		return null;
	}

	public String lambdaHandler(Map<Object, Object> event, Context context) {
		String geturi = null;
		String posturi = null;
		String puturi = null;
		String deleteuri = null;
		LambdaLogger logger = context.getLogger();
		String retval = null;
		StringBuilder result = new StringBuilder("");
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);

		for (Map.Entry<Object, Object> entry : event.entrySet()) {
			String key = (String)entry.getKey();
			String value = (String)entry.getValue();
			result.append(" Key = " + entry.getKey() + ", Value = " + entry.getValue());
			
			if (key.equals("requestType") && requestMap.containsKey(value)) {
				Request obj = requestMap.get(value);
				key = obj.type;
				value = obj.url;
			}
			
			if (key.equals("GET")) {
				geturi = value;
			} else if (key.equals("POST")) {
				posturi = value;
			} else if (key.equals("PUT")) {
				puturi = value;
			} else if (key.equals("DELETE")) {
				deleteuri = value;
			} else {
				nameValuePairs.add(new BasicNameValuePair(key,value));
			}
		}

		if (geturi != null) {
			HttpGet request = new HttpGet(geturi);
			retval = httpSendAndRecv(request);
		} else if (posturi != null) {
			HttpPost post = new HttpPost(posturi);
			try {
				post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				retval = httpSendAndRecv(post);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else if (puturi != null) {
			HttpPut put = new HttpPut(puturi);
			try {
				put.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				retval = httpSendAndRecv(put);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else if (deleteuri != null) {
			HttpDelete delete = new HttpDelete(deleteuri);
			retval = httpSendAndRecv(delete);
		}
		if (retval != null && retval.length() != 0) {
			logger.log("HTTP Response " + retval);
			return retval;
		} else {
			logger.log("Non HTTP Response " + result.toString());
			logger.log(result.toString());
			return result.toString();
		}
	}

	@Override
	public String handleRequest(SQSEvent event, Context context) {
		LambdaLogger logger = context.getLogger();
		StringBuilder retval = new StringBuilder("");
		for (SQSMessage msg : event.getRecords()) {
			String json = new String(msg.getBody());
			ObjectMapper mapper = new ObjectMapper();
			logger.log(json);
			retval.append(json);
			retval.append(" ");
			try {
				@SuppressWarnings("unchecked")
				Map<Object, Object> map = mapper.readValue(json, Map.class);
				return lambdaHandler(map,context);
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
		return retval.toString();
	}
}
