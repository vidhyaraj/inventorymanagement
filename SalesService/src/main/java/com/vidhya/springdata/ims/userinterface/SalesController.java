package com.vidhya.springdata.ims.userinterface;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.vidhya.springdata.ims.domain.Orders;
import com.vidhya.springdata.ims.domain.SalesService;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class SalesController {

	@Autowired
	SalesService salesService;

	@GetMapping(value = "/orders")
	public List<Orders> getProducts() {
		return salesService.getProducts();
	}

	@RequestMapping(value = "/order/{id}")
	public ResponseEntity<Orders> getProduct(@PathVariable("id") int id) {
		Orders obj = salesService.getProduct(id);
		if (obj != null)
			return new ResponseEntity<>(obj, HttpStatus.OK);
		else
			return new ResponseEntity<>(obj, HttpStatus.NO_CONTENT);
	}

	@PostMapping(value = "/order/create")
	public ResponseEntity<String> updateProduct(@RequestParam(value = "pname") String productName,
			@RequestParam(value = "quant") int quantity, @RequestParam(value = "custname") String custName) {
		if (salesService.updateProduct(productName, quantity, custName))
			return new ResponseEntity<>("Created order for " + custName, HttpStatus.OK);
		return new ResponseEntity<>("Unabled to create order for " + custName, HttpStatus.OK);
	}

	@RequestMapping(value = "/order/delete")
	public ResponseEntity<String> deleteOrder(@RequestParam("id") int id) {
		if (salesService.deleteOrder(id)) {
			return new ResponseEntity<>("Order deleted", HttpStatus.OK);
		}
		return new ResponseEntity<>("Order id not found", HttpStatus.OK);
	}
}
