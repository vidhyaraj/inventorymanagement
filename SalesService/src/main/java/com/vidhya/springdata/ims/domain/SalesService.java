package com.vidhya.springdata.ims.domain;

import java.util.List;

public interface SalesService {
	public List<Orders> getProducts();
	public Orders getProduct(int id);
	public boolean updateProduct(String productName, int quantity, String custName);
	public boolean deleteOrder(int orderId);
}
