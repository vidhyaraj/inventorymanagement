package com.vidhya.springdata.ims.domain;

import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vidhya.springdata.ims.infrastructure.OrderRepository;
import com.vidhya.springdata.ims.infrastructure.SalesClient;

@Service
public class SalesServiceImpl implements SalesService {
	private static Logger logger = LogManager.getLogger(SalesServiceImpl.class);
	private OrderRepository salesRepo;

	@Autowired
	SalesClient salesClient;

	public SalesServiceImpl(OrderRepository prepo) {
		this.salesRepo = prepo;
	}

	/**
	 * @return List<Orders>
	 */
	public List<Orders> getProducts() {
		return salesRepo.findAll();
	}

	/**
	 * @param id
	 * @return Orders
	 */
	public Orders getProduct(int id) {
		if (salesRepo.existsById(id)) {
			Optional<Orders> value = salesRepo.findById(id);
			if(value.isPresent()) return value.get();
		}
		return null;
	}

	/**
	 * @param productName
	 * @param quantity
	 * @param customerName
	 * @return boolean
	 */
	public boolean updateProduct(String productName, int quantity, String custName) {
		logger.info("Finding Id Product id : {}",productName);
		Orders supobj = new Orders();
		supobj.setCustomerName(custName);
		/* Updating details via feign client purchase service*/
		String data = salesClient.update(productName, quantity);
		if(data == null || data.contains("Failed")) {
			return false;
		}
		logger.info("Response String : {}",data);
		salesRepo.save(supobj);
		return true;
	}

	/**
	 * @param orderId
	 * @return boolean
	 */
	public boolean deleteOrder(int orderId) {
		if(salesRepo.existsById(orderId)) {
			salesRepo.deleteById(orderId);
			return true;
		}
		return false;
	}

}
