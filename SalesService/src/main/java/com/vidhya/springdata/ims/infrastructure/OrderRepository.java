package com.vidhya.springdata.ims.infrastructure;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vidhya.springdata.ims.domain.Orders;

public interface OrderRepository extends JpaRepository<Orders, Integer> {

}
