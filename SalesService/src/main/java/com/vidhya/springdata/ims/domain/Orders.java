package com.vidhya.springdata.ims.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="orders")
public class Orders {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int oid;

	@Column(name = "customer_name")
	private String customerName;
	
	public int getOid() {
		return oid;
	}

	public void setOid(int pid) {
		this.oid = pid;
	}
	
	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String cName) {
		this.customerName = cName;
	}
}
