package com.vidhya.springdata.ims.infrastructure;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient("PURCHASE-SERVICE")
public interface SalesClient {
    @PutMapping(value = "/inventoryManagement/purchase/api/stocks/updatebyname")
    String update(@RequestParam String stockName,@RequestParam int quantity);
}
