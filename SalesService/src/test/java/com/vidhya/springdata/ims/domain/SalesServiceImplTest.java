package com.vidhya.springdata.ims.domain;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.vidhya.springdata.ims.infrastructure.OrderRepository;
import com.vidhya.springdata.ims.infrastructure.SalesClient;

@RunWith(MockitoJUnitRunner.class)
public class SalesServiceImplTest {
	
	@Mock
	OrderRepository orderRepository;
	
	@InjectMocks
	SalesServiceImpl impl;
	
	@Mock
	SalesClient salesClients;

	@Test
	public void testGetProducts() {
		List<Orders> list = new ArrayList<Orders>();
		Orders orders = new Orders();
		orders.setCustomerName("Rajesh");
		orders.setOid(1);
		list.add(orders);

		when(orderRepository.findAll()).thenReturn(list);

		List<Orders> orderList = impl.getProducts();

		assertEquals(list, orderList);
	}
	
	@Test
	public void testGetProduct_byId() {
		Orders orders = new Orders();
		orders.setCustomerName("Rajesh");
		orders.setOid(1);

		when(orderRepository.existsById(1)).thenReturn(true);
		when(orderRepository.findById(1)).thenReturn(Optional.of(orders));

		Orders orderObj = impl.getProduct(1);

		assertEquals(orders.getCustomerName(), orderObj.getCustomerName());
		assertEquals(orders.getOid(), orderObj.getOid());
	}
	
	@Test
	public void testGetProduct_IdNotExist() {
		when(orderRepository.existsById(1)).thenReturn(false);

		Orders orderObj = impl.getProduct(1);

		assertEquals(null, orderObj);
	}
	
	@Test
	public void testDeleteOrder_True() {
		List<Orders> list = new ArrayList<Orders>();
		Orders orders = new Orders();
		orders.setCustomerName("Rajesh");
		orders.setOid(1);
		list.add(orders);

		when(orderRepository.existsById(1)).thenReturn(true);

		boolean result = impl.deleteOrder(1);

		assertEquals(true, result);
	}

	@Test
	public void testDeleteProduct_False_notSameId() {
		List<Orders> list = new ArrayList<Orders>();
		Orders orders = new Orders();
		orders.setCustomerName("Rajesh");
		orders.setOid(1);
		list.add(orders);

		boolean result = impl.deleteOrder(2);

		assertEquals(false, result);
	}

	@Test
	public void testDeleteProduct_False() {
		boolean result = impl.deleteOrder(2);
		assertEquals(false, result);
	}
	
	@Test
	public void testUpdateProduct() {
		impl.salesClient = salesClients;
		boolean result = impl.updateProduct("Chair", 10, "Rajesh");
		assertEquals(false, result);
	}

}
