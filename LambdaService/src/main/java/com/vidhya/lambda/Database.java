package com.vidhya.lambda;

import java.sql.Connection;
import java.sql.SQLException;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
public final class Database {

	private Database() {
        throw new AssertionError();
    }

    private static final HikariDataSource dataSource;

    static {

        final HikariConfig config = new HikariConfig();
        config.setDriverClassName("com.mysql.cj.jdbc.Driver");
        config.setJdbcUrl("jdbc:mysql://database-1.cychkgoslonz.us-east-2.rds.amazonaws.com:3306/ims_db");
        config.setUsername("root");
        config.setPassword("evHN0DwpVLyJVjxuxJH0");

        dataSource = new HikariDataSource(config);
    }

    public static Connection connection() throws SQLException {
        return dataSource.getConnection();
    }
}