package com.vidhya.lambda;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

public class LambdaService implements RequestHandler<Void, List<Supplier>> {

	@Override
	public List<Supplier> handleRequest(Void input, Context context) {
		final List<Supplier> employees = new ArrayList<>();

		try (Connection conn = Database.connection();
				PreparedStatement ps = conn.prepareStatement("SELECT * FROM supplier")) {

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				Supplier employee = new Supplier();
				employee.setSupplierName(rs.getString("supplier_name"));
				employee.setSid(rs.getInt("sid"));

				employees.add(employee);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return employees;
	}
}
