package com.vidhya.lambda;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="supplier")
@JsonIgnoreProperties({"hibernateLazyInitializer","handler","materials"})
public class Supplier {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int sid;

	@Column(name = "supplier_name")
	private String supplierName;

	@OneToMany(mappedBy = "supplier", cascade = CascadeType.ALL)
	private Set<Stocks> materials;
	
	public int getSid() {
		return sid;
	}

	public void setSid(int sid) {
		this.sid = sid;
	}
	
	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}
	
	public Set<Stocks> getMaterials() {
		return materials;
	}

	public void setMaterials(Set<Stocks> materials) {
		this.materials = materials;
	}

	public void addMaterials(Stocks material) {
		if (material != null) {
			if(materials == null ) {
				materials = new HashSet<>();
			}
			material.setSplr(this);
			materials.add(material);
		}
	}

}
