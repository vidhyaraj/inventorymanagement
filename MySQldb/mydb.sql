create database ims_db;
use ims_db;

-- drop table rawmaterial;
-- drop table materials;
-- drop table supplier;
-- drop table production;
-- drop table product;
-- drop table orders;

-- purchase service - for storing supplier details
create table supplier(
sid int primary key auto_increment,
supplier_name varchar(10)
);

-- purchase service - for storing stocks ( Raw materials and Finished product )
-- like Inventory
create table materials(
mid int primary key auto_increment,
mname varchar(10),
quantity int not null,
mtype varchar(10),
supplier_id int, 
foreign key (supplier_id)
references supplier(sid),
);

-- production service - store product details for manufacture
create table product(
pid int primary key auto_increment,
product_name varchar(10)
);

-- production service - per product's raw materials details
create table rawmaterial (
rawid int primary key auto_increment,
stockid int,
quantity int,
product_id int, 
foreign key (product_id)
references product(pid)
);

-- production service - Manufactured products details
create table production(
pid int primary key auto_increment,
product_name varchar(10)
);

-- Sales service - Sold products details
create table orders (
oid int primary key auto_increment,
customer_name varchar(10)
);


-- select * from rawmaterial;
-- select * from materials;
-- select * from supplier;
-- select * from production;
-- select * from product;
-- select * from orders;