package com.vidhya.springdata.ims;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class EurekaServerApplicationTests {
	
	@Test
	public void testMain() {
		EurekaServerApplication.main(new String[] {});
	    Assert.assertTrue(true);
	}

}
