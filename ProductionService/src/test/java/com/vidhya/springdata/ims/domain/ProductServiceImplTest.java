package com.vidhya.springdata.ims.domain;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.vidhya.springdata.ims.infrastructure.ProductRepository;
import com.vidhya.springdata.ims.infrastructure.PurchaseClient;
import com.vidhya.springdata.ims.infrastructure.RawMaterialRepository;

@RunWith(MockitoJUnitRunner.class)
public class ProductServiceImplTest {

	@Mock
	RawMaterialRepository rawMaterialRepository;
	@Mock
	ProductRepository productRepository;
	@Mock
	PurchaseClient purchaseClient;
	
	@InjectMocks
	ProductServiceImpl impl;

	@Test
	public void testGetRawMaterials() {
		List<RawMaterial> list = new ArrayList<RawMaterial>();
		RawMaterial material = new RawMaterial();
		material.setStockid(10);
		material.setQuantity(100);
		list.add(material);

		when(rawMaterialRepository.findAll()).thenReturn(list);

		List<RawMaterial> materialList = impl.getRawMaterials();

		assertEquals(list, materialList);
	}
	
	@Test
	public void testGetProducts() {
		List<Product> list = new ArrayList<Product>();
		Product product = new Product();
		product.setPid(1);
		product.setProductName("Chair");
		list.add(product);

		when(productRepository.findAll()).thenReturn(list);

		List<Product> productList = impl.getProducts();

		assertEquals(list, productList);
	}
	
	@Test
	public void testGetProduct_byId() {
		Product product = new Product();
		product.setPid(1);
		product.setProductName("Chair");

		when(productRepository.existsById(1)).thenReturn(true);
		when(productRepository.findById(1)).thenReturn(Optional.of(product));

		Product productObj = impl.getProduct(1);

		assertEquals(product, productObj);
	}
	
	@Test
	public void testGetProduct_IdNotExist() {
		when(productRepository.existsById(1)).thenReturn(false);

		Product productObj = impl.getProduct(1);

		assertEquals(null, productObj);
	}
	
	@Test
	public void testGetProductByName() {
		List<Product> list = new ArrayList<Product>();
		Product product = new Product();
		product.setPid(1);
		product.setProductName("Chair");
		list.add(product);

		when(productRepository.findAll()).thenReturn(list);

		Product productObj = impl.getProductByName("Chair");

		assertEquals(product, productObj);
	}

	@Test
	public void testGetProductByName_null_listEmpty() {
		List<Product> list = new ArrayList<Product>();
		when(productRepository.findAll()).thenReturn(list);
		Product productObj = impl.getProductByName("Chair");
		assertEquals(null, productObj);
	}

	@Test
	public void testGetProductByName_null_notInList() {
		List<Product> list = new ArrayList<Product>();
		Product product = new Product();
		product.setPid(1);
		product.setProductName("Chair");
		list.add(product);

		when(productRepository.findAll()).thenReturn(list);

		Product productObj = impl.getProductByName("Table");

		assertEquals(null, productObj);
	}
	
	@Test
	public void testCreateProduct() {
		List<Product> list = new ArrayList<Product>();
		
		Materials material = new Materials();
		material.setMid(10);
		material.setType("Plastic");
		
		RawMaterial rawMaterial = new RawMaterial();
		rawMaterial.setStockid(10);
		
		Product product = new Product();
		product.setPid(1);
		product.setProductName("Chair");
		product.addMaterials(rawMaterial);
		
		list.add(product);
		
		when(productRepository.findAll()).thenReturn(list);
		impl.purchaseClient = purchaseClient;
		boolean result = impl.createProduct("Chair", "Plastic", 10);
		assertEquals(true, result);
	}
	
	@Test
	public void testDeleteProduct_True() {
		List<Product> list = new ArrayList<Product>();
		Product product = new Product();
		product.setPid(1);
		product.setProductName("Chair");
		list.add(product);

		when(productRepository.findAll()).thenReturn(list);

		boolean result = impl.deleteProduct("Chair");

		assertEquals(true, result);
	}

	@Test
	public void testDeleteProduct_False_notSameName() {
		List<Product> list = new ArrayList<Product>();
		Product product = new Product();
		product.setPid(1);
		product.setProductName("Chair");
		list.add(product);

		when(productRepository.findAll()).thenReturn(list);

		boolean result = impl.deleteProduct("Wood");

		assertEquals(false, result);
	}

	@Test
	public void testDeleteProduct_False() {
		List<Product> list = new ArrayList<Product>();
		when(productRepository.findAll()).thenReturn(list);
		boolean result = impl.deleteProduct("Chair");
		assertEquals(false, result);
	}
}
