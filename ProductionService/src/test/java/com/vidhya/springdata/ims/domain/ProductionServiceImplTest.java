package com.vidhya.springdata.ims.domain;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.vidhya.springdata.ims.infrastructure.ProductRepository;
import com.vidhya.springdata.ims.infrastructure.ProductionRepository;
import com.vidhya.springdata.ims.infrastructure.PurchaseClient;

@RunWith(MockitoJUnitRunner.class)
public class ProductionServiceImplTest {

	@Mock
	ProductionRepository productionRepository;
	@Mock
	ProductRepository productRepository;
	@Mock
	PurchaseClient purchaseClient;

	@InjectMocks
	ProductionServiceImpl impl;

	@Test
	public void testGetProducStocksObj() {
		List<Product> list = new ArrayList<Product>();
		Product product = new Product();
		product.setPid(1);
		product.setProductName("Chair");
		list.add(product);

		when(productRepository.findAll()).thenReturn(list);

		Product productObj = impl.getProducStocksObj("Chair");

		assertEquals(product.getProductName(), productObj.getProductName());
		assertEquals(product.getPid(), productObj.getPid());
	}

	@Test
	public void testGetProducStocksObj_null_listEmpty() {
		List<Product> list = new ArrayList<Product>();
		when(productRepository.findAll()).thenReturn(list);
		Product productObj = impl.getProducStocksObj("Chair");
		assertEquals(null, productObj);
	}

	@Test
	public void testGetProducStocksObj_null_notInList() {
		List<Product> list = new ArrayList<Product>();
		Product product = new Product();
		product.setPid(1);
		product.setProductName("Chair");
		list.add(product);

		when(productRepository.findAll()).thenReturn(list);

		Product productObj = impl.getProducStocksObj("Table");

		assertEquals(null, productObj);
	}

	@Test
	public void testGetProducts() {
		List<Production> list = new ArrayList<Production>();
		Production production = new Production();
		production.setPid(1);
		production.setProductName("Chair");
		list.add(production);

		when(productionRepository.findAll()).thenReturn(list);

		List<Production> productionList = impl.getProducts();

		assertEquals(list, productionList);
	}

	@Test
	public void testGetProduct_byId() {
		Production production = new Production();
		production.setPid(1);
		production.setProductName("Chair");

		when(productionRepository.existsById(1)).thenReturn(true);
		when(productionRepository.findById(1)).thenReturn(Optional.of(production));

		Production productionObj = impl.getProduct(1);

		assertEquals(production.getPid(), productionObj.getPid());
	}
	
	@Test
	public void testGetProduct_IdNotExist() {
		when(productionRepository.existsById(1)).thenReturn(false);

		Production productionObj = impl.getProduct(1);

		assertEquals(null, productionObj);
	}
	
	@Test
	public void testUpdateProduct_True() {
		List<Product> list = new ArrayList<Product>();
		
		RawMaterial material = new RawMaterial();
		material.setStockid(10);
		material.setQuantity(100);
		
		Product product = new Product();
		product.setPid(1);
		product.setProductName("Chair");
		product.addMaterials(material);
		list.add(product);
		
		material.getSplr();

		when(productRepository.findAll()).thenReturn(list);
		
		impl.purchaseClient = purchaseClient;
		
		boolean result = impl.updateProduct("Chair");
		assertEquals(false, result);
	}
	
	@Test
	public void testUpdateProduct_False() {
		List<Product> list = new ArrayList<Product>();

		when(productRepository.findAll()).thenReturn(list);
		
		impl.purchaseClient = purchaseClient;
		
		boolean result = impl.updateProduct("Chair");
		assertEquals(false, result);
	}

	@Test
	public void testDeleteProduct_True() {
		List<Production> list = new ArrayList<Production>();
		Production production1 = new Production();
		production1.setPid(1);
		production1.setProductName("Chair");
		Production production2 = new Production();
		production2.setPid(2);
		production2.setProductName("Table");
		list.add(production1);
		list.add(production2);

		when(productionRepository.findAll()).thenReturn(list);

		boolean result = impl.deleteProduct("Chair");

		assertEquals(true, result);
	}

	@Test
	public void testDeleteProduct_False_notSameName() {
		List<Production> list = new ArrayList<Production>();
		Production production1 = new Production();
		production1.setPid(1);
		production1.setProductName("Chair");
		Production production2 = new Production();
		production2.setPid(2);
		production2.setProductName("Table");
		list.add(production1);
		list.add(production2);

		when(productionRepository.findAll()).thenReturn(list);

		boolean result = impl.deleteProduct("Wood");

		assertEquals(false, result);
	}

	@Test
	public void testDeleteProduct_False() {
		List<Production> list = new ArrayList<Production>();
		when(productionRepository.findAll()).thenReturn(list);
		boolean result = impl.deleteProduct("Chair");
		assertEquals(false, result);
	}

}
