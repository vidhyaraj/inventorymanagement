package com.vidhya.springdata.ims.domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="product")
@JsonIgnoreProperties({"hibernateLazyInitializer","handler","materials"})
public class Product {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int pid;

	@Column(name = "product_name")
	private String productName;

	@OneToMany(mappedBy = "productids", cascade = CascadeType.ALL)
	private Set<RawMaterial> materials = new HashSet<>();
	
	public int getPid() {
		return pid;
	}

	public void setPid(int pid) {
		this.pid = pid;
	}
	
	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}
	
	public Set<RawMaterial> getMaterials() {
		return materials;
	}

	public void addMaterials(RawMaterial material) {
		if (material != null) {
			material.setSplr(this);
			materials.add(material);
		}
	}

}
