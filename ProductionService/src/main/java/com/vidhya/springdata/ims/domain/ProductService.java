package com.vidhya.springdata.ims.domain;

import java.util.List;

public interface ProductService {
	public List<RawMaterial> getRawMaterials();
	public List<Product> getProducts();
	public Product getProduct(int id);
	public boolean createProduct(String productName, String stockName, int quant);
	public boolean deleteProduct(String productName);
}
