package com.vidhya.springdata.ims.infrastructure;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vidhya.springdata.ims.domain.Product;

public interface ProductRepository extends JpaRepository<Product, Integer> {

}
