package com.vidhya.springdata.ims.infrastructure;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vidhya.springdata.ims.domain.Production;

public interface ProductionRepository extends JpaRepository<Production, Integer> {

}
