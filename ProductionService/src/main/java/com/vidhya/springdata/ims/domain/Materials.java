package com.vidhya.springdata.ims.domain;

public class Materials {

	private int mid;
	private int quantity;
	private String type;
    private Production product;

	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getMid() {
		return mid;
	}

	public void setMid(int mid) {
		this.mid = mid;
	}

	
	public Production getSplr() {
		return product;
	}

	public void setSplr(Production splr) {
		this.product = splr;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
}
