package com.vidhya.springdata.ims.domain;

import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vidhya.springdata.ims.infrastructure.ProductRepository;
import com.vidhya.springdata.ims.infrastructure.ProductionRepository;
import com.vidhya.springdata.ims.infrastructure.PurchaseClient;

@Service
public class ProductionServiceImpl implements ProductionService {
	private ProductionRepository productionRepo;
	private ProductRepository productRepo;
	private static Logger logger = LogManager.getLogger(ProductionServiceImpl.class);
	
	@Autowired
	PurchaseClient purchaseClient;

	public ProductionServiceImpl(ProductionRepository prepo, ProductRepository pdrepo) {
		this.productionRepo = prepo;
		this.productRepo = pdrepo;
	}

	/**
	 * @param productName
	 * @return Product
	 */
	public Product getProducStocksObj(String productName) {
			List<Product> obj = productRepo.findAll();
			/* Iterating list to find the obj */
			for (Product temp : obj) {
				if (temp.getProductName() != null && temp.getProductName().equals(productName)) {
					logger.info(temp);
					return temp;
				}
			}
		return null;
	}

	/**
	 * @return List<Production>
	 */
	public List<Production> getProducts() {
		return productionRepo.findAll();
	}

	/**
	 * @param id
	 * @return Production
	 */
	public Production getProduct(int id) {
		if (productionRepo.existsById(id)) {
			Optional<Production> value = productionRepo.findById(id);
			if(value.isPresent()) return value.get();
		}
		return null;
	}

	/**
	 * @param productName
	 * @return boolean
	 */
	public boolean updateProduct(String productName) {
		Production productionObj = new Production();
		logger.info("Finding ProductName : {}", productName);
		Product pobj = getProducStocksObj(productName);
		if (pobj != null) {
			logger.info("Found Product : {}",productName);
			/* Do production here */
			for (RawMaterial robj : pobj.getMaterials()) {
				String data = purchaseClient.updateStock(robj.getStockid(),robj.getQuantity());
				if(data == null || data.contains("Failed")) {
					return false;
				}
				logger.info("Purchase Response : {}",data);
			}
			String data = purchaseClient.addStock(pobj.getProductName(),1);
			logger.info("Added Sales : {}",data);
			productionObj.setProductName(pobj.getProductName());
			productionRepo.save(productionObj);
			return true;
		} 
		return false;

	}

	/**
	 * @param productName
	 * @return boolean
	 */
	@Override
	public boolean deleteProduct(String productName) {
		List<Production> productionList = getProducts();
		for(Production productionObj : productionList) {
			if(productionObj.getProductName().equals(productName)) {
				productionRepo.delete(productionObj);
				return true;
			}
		}
		return false;
	}
}
