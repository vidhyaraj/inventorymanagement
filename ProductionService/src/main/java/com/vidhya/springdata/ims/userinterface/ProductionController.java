package com.vidhya.springdata.ims.userinterface;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.vidhya.springdata.ims.domain.Production;
import com.vidhya.springdata.ims.domain.ProductionService;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class ProductionController {

	@Autowired
	ProductionService productionService;

	@GetMapping(value = "/productions")
	public List<Production> getProducts() {
		return productionService.getProducts();
	}

	@RequestMapping(value = "/productions/{id}")
	public Production getProduct(@PathVariable("id") int id) {
		return productionService.getProduct(id);
	}

	@PostMapping(value = "/production/start")
	public ResponseEntity<String> updateProduct(@RequestParam(value = "pname") String productName) {
		if (productionService.updateProduct(productName))
			return new ResponseEntity<>("Produced product " + productName, HttpStatus.OK);
		return new ResponseEntity<>("Invalid product " + productName, HttpStatus.OK);
	}

	@DeleteMapping(value = "/production/delete")
	public ResponseEntity<String> deleteProduct(@RequestParam String productName) {
		if (productionService.deleteProduct(productName))
			return new ResponseEntity<>("Deleted Production Entry for " + productName, HttpStatus.OK);
		return new ResponseEntity<>("Entry not found", HttpStatus.OK);
	}
}
