package com.vidhya.springdata.ims.infrastructure;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vidhya.springdata.ims.domain.RawMaterial;

public interface RawMaterialRepository extends JpaRepository<RawMaterial, Integer>{

}
