package com.vidhya.springdata.ims.domain;

import java.util.List;
import java.util.Set;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vidhya.springdata.ims.infrastructure.ProductRepository;
import com.vidhya.springdata.ims.infrastructure.PurchaseClient;
import com.vidhya.springdata.ims.infrastructure.RawMaterialRepository;

@Service
public class ProductServiceImpl implements ProductService {
	private ProductRepository pdtRepo;
	private RawMaterialRepository rawRepo;
	private static Logger logger = LogManager.getLogger(ProductServiceImpl.class);

	@Autowired
	PurchaseClient purchaseClient;

	public ProductServiceImpl(ProductRepository pdrepo, RawMaterialRepository rrepo) {
		this.pdtRepo = pdrepo;
		this.rawRepo = rrepo;
	}

	/**
	 * @return List<RawMaterial>
	 */
	public List<RawMaterial> getRawMaterials() {
		logger.info("Finding rawRepo ");
		return rawRepo.findAll();
	}

	/**
	 * @return List<Product>
	 */
	public List<Product> getProducts() {
		return pdtRepo.findAll();
	}

	/**
	 * @param id
	 * @return Product
	 */
	public Product getProduct(int id) { 
		if (pdtRepo.existsById(id)) {
			Optional<Product> value = pdtRepo.findById(id);
			if(value.isPresent()) return value.get();
		}
		return null;
	}
	
	/**
	 * @param name
	 * @return Product
	 */
	public Product getProductByName(String name) {
		for(Product obj : pdtRepo.findAll()) {
			if(obj.getProductName()!=null && obj.getProductName().equals(name)) {
				return obj;
			}
		}
		return null;
	}

	/**
	 * @param productName
	 * @param stockName
	 * @param quantity
	 * @return boolean
	 */
	public boolean createProduct(String productName, String stockName, int quant) {
		Product productObj;
		boolean isupdated = false;

		logger.info("ProductName {}", productName);
		logger.info("RawMaterialName {}", stockName);
		logger.info("Quantity {}", quant);

		Materials temp = purchaseClient.getStockName(stockName);
		if (temp == null) {
			logger.info("Rawmaterial {} not found in stocks", stockName);
		}

		productObj = getProductByName(productName);
		if (productObj == null) {
			productObj = new Product();
		}

		productObj.setProductName(productName);
		Set<RawMaterial> rawMaterials = productObj.getMaterials();

		for (RawMaterial obj : rawMaterials) {
			if (temp != null && obj.getStockid() == temp.getMid()) {
				logger.info("Updating Stock id {} for quant {}", obj.getStockid(), quant);
				obj.setQuantity(quant);
				isupdated = true;
				break;
			}
		}
		if (!isupdated && temp != null) {
			RawMaterial obj = new RawMaterial();
			obj.setQuantity(quant);
			obj.setStockid(temp.getMid());
			productObj.addMaterials(obj);
		}
		pdtRepo.save(productObj);
		return true;
	}

	/**
	 * @param productName
	 * @return boolean
	 */
	public boolean deleteProduct(String productName) {
		List<Product> productList = getProducts();
		/* Iterate list to find product to delete */
		for (Product product : productList) {
			if (product.getProductName().equals(productName)) {
				pdtRepo.delete(product);
				return true;
			}
		}
		return false;

	}

}
