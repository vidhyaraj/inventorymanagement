package com.vidhya.springdata.ims.domain;

import java.util.List;

public interface ProductionService {
	public List<Production> getProducts();
	public Production getProduct(int id);
	public boolean updateProduct(String productName);
	public boolean deleteProduct(String productName);
}
