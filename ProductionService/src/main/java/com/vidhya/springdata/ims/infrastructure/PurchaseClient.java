package com.vidhya.springdata.ims.infrastructure;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.vidhya.springdata.ims.domain.Materials;

@FeignClient("PURCHASE-SERVICE")
public interface PurchaseClient {
    @PutMapping(value = "/inventoryManagement/purchase/api/stocks/update")
    String updateStock(@RequestParam int stockid,@RequestParam int quantity);

    @PutMapping(value = "/inventoryManagement/purchase/api/stocks/add")
    String addStock(@RequestParam String stockName,@RequestParam int quantity);
    
    @GetMapping(value="/inventoryManagement/purchase/api/stocks/{id}")
    Materials getStock(@PathVariable int id);
    
    @GetMapping(value="/inventoryManagement/purchase/api/stocks/name")
    Materials getStockName(@RequestParam String stockName);

}
