package com.vidhya.springdata.ims.userinterface;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.vidhya.springdata.ims.domain.Product;
import com.vidhya.springdata.ims.domain.ProductService;
import com.vidhya.springdata.ims.domain.RawMaterial;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class ProductController {
	
	private static Logger logger = LogManager.getLogger(ProductController.class);

	@Autowired
	ProductService productService;

	@GetMapping(value = "/rawmaterials")
	public List<RawMaterial> getRawMaterials() {
		logger.info("Finding rawRepo ");
		return productService.getRawMaterials();
	}

	@GetMapping(value = "/products")
	public List<Product> getProducts() {
		return productService.getProducts();
	}

	@RequestMapping(value = "/products/{id}")
	public Product getProduct(@PathVariable("id") int id) {
		return productService.getProduct(id);
	}

	@PostMapping(value = "/product/create")
	public ResponseEntity<String> createProduct(@RequestParam(value = "pname") String productName,
			@RequestParam(value = "stockName") String stockName, @RequestParam(value = "quant") int quant) {
		if (productService.createProduct(productName, stockName, quant))
			return ResponseEntity.status(HttpStatus.OK).body("Created new Product entry for  " + productName);
		return ResponseEntity.status(HttpStatus.OK).body("Rawmaterial " + stockName + " not found in stocks");
	}

	@DeleteMapping(value = "/product/delete")
	public ResponseEntity<String> deleteProduct(@RequestParam(value = "pname") String productName) {
		if (productService.deleteProduct(productName))
			return ResponseEntity.status(HttpStatus.OK).body("Deleted Product entry for  " + productName);
		return ResponseEntity.status(HttpStatus.OK).body("Product entry not found  " + productName);
	}
}
