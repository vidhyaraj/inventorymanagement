package com.vidhya.springdata.ims.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="rawmaterial")
public class RawMaterial {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int rawid;

	@Column(name = "quantity")
	private int quantity;
	
	@Column(name = "stockid")
	private int stockid;
	
	@ManyToOne
    @JoinColumn(name="product_id")
    private Product productids;

	public int getStockid() {
		return stockid;
	}

	public void setStockid(int stockid) {
		this.stockid = stockid;
	}
	
	public Product getSplr() {
		return productids;
	}

	public void setSplr(Product splr) {
		this.productids = splr;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
}
