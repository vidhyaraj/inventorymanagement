package com.vidhya.springdata.ims.domain;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.vidhya.springdata.ims.infrastructure.SupplierRepository;

@RunWith(MockitoJUnitRunner.class)
public class PurchaseServiceImplTest {
	
	@Mock
	SupplierRepository supplierRepository;
	
	@InjectMocks
	PurchaseServiceImpl impl;
	
	@Test
	public void testGetSupplierByName() {
		List<Supplier> list = new ArrayList<Supplier>();
		Supplier supplier = new Supplier();
		supplier.setSupplierName("Rajesh");
		list.add(supplier);

		when(supplierRepository.findAll()).thenReturn(list);

		Supplier supplierObj = impl.getSupplierByName("Rajesh");

		assertEquals(supplier.getSid(), supplierObj.getSid());
	}
	
	@Test
	public void testGetSupplierByName_null_listEmpty() {
		List<Supplier> list = new ArrayList<Supplier>();
		when(supplierRepository.findAll()).thenReturn(list);
		Supplier supplierObj = impl.getSupplierByName("Rajesh");
		assertEquals(null, supplierObj);
	}

	@Test
	public void testGetSupplierByName_null_notInList() {
		List<Supplier> list = new ArrayList<Supplier>();
		Supplier supplier = new Supplier();
		supplier.setSupplierName("Rajesh");
		list.add(supplier);

		when(supplierRepository.findAll()).thenReturn(list);

		Supplier supplierObj = impl.getSupplierByName("Raj");

		assertEquals(null, supplierObj);
	}
	
	@Test
	public void testGetSuppliers() {
		List<Supplier> list = new ArrayList<Supplier>();
		Supplier supplier = new Supplier();
		supplier.setSupplierName("Rajesh");
		list.add(supplier);

		when(supplierRepository.findAll()).thenReturn(list);

		List<Supplier> supplierList = impl.getSupplier();

		assertEquals(list, supplierList);
	}
	
	@Test
	public void testGetSupplier_byId() {
		Supplier supplier = new Supplier();
		supplier.setSid(1);
		supplier.setSupplierName("Rajesh");

		when(supplierRepository.existsById(1)).thenReturn(true);
		when(supplierRepository.findById(1)).thenReturn(Optional.of(supplier));

		Supplier suppierObj = impl.getSupplier(1);

		assertEquals(supplier, suppierObj);
	}
	
	@Test
	public void testGetProduct_IdNotExist() {
		when(supplierRepository.existsById(1)).thenReturn(false);

		Supplier suppierObj = impl.getSupplier(1);

		assertEquals(null, suppierObj);
	}
	
	@Test
	public void testDeleteProduct_True() {
		List<Supplier> list = new ArrayList<Supplier>();
		Supplier supplier = new Supplier();
		supplier.setSupplierName("Rajesh");
		list.add(supplier);

		when(supplierRepository.findAll()).thenReturn(list);

		boolean result = impl.deleteSupplier("Rajesh");

		assertEquals(true, result);
	}

	@Test
	public void testDeleteProduct_False_notSameName() {
		List<Supplier> list = new ArrayList<Supplier>();
		Supplier supplier = new Supplier();
		supplier.setSupplierName("Rajesh");
		list.add(supplier);

		when(supplierRepository.findAll()).thenReturn(list);

		boolean result = impl.deleteSupplier("Raj");

		assertEquals(false, result);
	}

	@Test
	public void testDeleteProduct_False() {
		List<Supplier> list = new ArrayList<Supplier>();
		when(supplierRepository.findAll()).thenReturn(list);
		boolean result = impl.deleteSupplier("Raj");
		assertEquals(false, result);
	}
	
	@Test
	public void testCreateSupplier() {
		impl.createSupplier("Rajesh", 10, "Raw", "Plastic");
		Assert.assertTrue(true);
	}
	
	@Test
	public void testUpdateSupplier() {
		PurchaseResponse response = impl.updateSupplier("Vidhya", 20, "Raw", "Iron");
		assertEquals(PurchaseResponse.CREATE_SUPPLIER, response);
		
	}

}
