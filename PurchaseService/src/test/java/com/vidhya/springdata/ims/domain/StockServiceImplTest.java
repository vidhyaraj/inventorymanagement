package com.vidhya.springdata.ims.domain;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.vidhya.springdata.ims.infrastructure.StocksRepository;

@RunWith(MockitoJUnitRunner.class)
public class StockServiceImplTest {

	@Mock
	StocksRepository stockRepository;
	
	@InjectMocks
	StocksServiceImpl impl;
	
	@Test
	public void testGetStockByName() {
		List<Stocks> list = new ArrayList<Stocks>();
		Stocks stock = new Stocks();
		stock.setMid(1);
		stock.setMaterialName("Iron");
		stock.setType("Raw");
		list.add(stock);

		when(stockRepository.findAll()).thenReturn(list);

		Stocks stockObj = impl.getStockByName("Iron");

		assertEquals(stock.getMid(), stockObj.getMid());
		assertEquals(stock.getType(), stockObj.getType());
	}
	
	@Test
	public void testGetStockByName_null_listEmpty() {
		List<Stocks> list = new ArrayList<Stocks>();
		when(stockRepository.findAll()).thenReturn(list);
		Stocks stockObj = impl.getStockByName("Iron");
		assertEquals(null, stockObj);
	}

	@Test
	public void testGetSupplierByName_null_notInList() {
		List<Stocks> list = new ArrayList<Stocks>();
		Stocks stock = new Stocks();
		stock.setMid(1);
		stock.setMaterialName("Iron");
		list.add(stock);

		when(stockRepository.findAll()).thenReturn(list);

		Stocks stockObj = impl.getStockByName("Plastic");

		assertEquals(null, stockObj);
	}
	
	@Test
	public void testGetStocks() {
		List<Stocks> list = new ArrayList<Stocks>();
		Stocks stock = new Stocks();
		stock.setMid(1);
		stock.setMaterialName("Iron");
		list.add(stock);

		when(stockRepository.findAll()).thenReturn(list);

		List<Stocks> stockList = impl.getStocks();

		assertEquals(list, stockList);
	}
	
	@Test
	public void testGetStocks_byId() {
		Stocks stock = new Stocks();
		stock.setMid(1);
		stock.setMaterialName("Iron");

		when(stockRepository.existsById(1)).thenReturn(true);
		when(stockRepository.findById(1)).thenReturn(Optional.of(stock));

		Stocks stockObj = impl.getStockById(1);

		assertEquals(stock, stockObj);
	}
	
	@Test
	public void testGetProduct_IdNotExist() {
		when(stockRepository.existsById(1)).thenReturn(false);

		Stocks stockObj = impl.getStockById(1);

		assertEquals(null, stockObj);
	}
	
	@Test
	public void testDeleteProduct_True() {
		List<Stocks> list = new ArrayList<Stocks>();
		Stocks stock = new Stocks();
		stock.setMid(1);
		stock.setMaterialName("Iron");
		list.add(stock);

		when(stockRepository.findAll()).thenReturn(list);

		boolean result = impl.deleteStock("Iron");

		assertEquals(true, result);
	}

	@Test
	public void testDeleteProduct_False_notSameName() {
		List<Stocks> list = new ArrayList<Stocks>();
		Stocks stock = new Stocks();
		stock.setMid(1);
		stock.setMaterialName("Iron");
		list.add(stock);

		when(stockRepository.findAll()).thenReturn(list);

		boolean result = impl.deleteStock("Plastic");

		assertEquals(false, result);
	}

	@Test
	public void testDeleteProduct_False() {
		List<Stocks> list = new ArrayList<Stocks>();
		when(stockRepository.findAll()).thenReturn(list);
		boolean result = impl.deleteStock("Raj");
		assertEquals(false, result);
	}
	
	@Test
	public void testGetStock() {
		List<Stocks> list = new ArrayList<Stocks>();
		Stocks stock = new Stocks();
		stock.setMid(1);
		stock.setMaterialName("Iron");
		list.add(stock);

		when(stockRepository.findAll()).thenReturn(list);

		Stocks stockObj = impl.getStock("Iron");

		assertEquals(stock, stockObj);
	}
	
	@Test
	public void testUpdateStockById() {
		PurchaseResponse response = impl.updateStock(10, 100);
		assertEquals(PurchaseResponse.STOCK_NOTFOUND, response);
	}
	
	@Test
	public void testUpdateStockById_List() {
		Stocks stock = new Stocks();
		stock.setMid(10);
		stock.setMaterialName("Iron");

		when(stockRepository.existsById(10)).thenReturn(true);
		when(stockRepository.findById(10)).thenReturn(Optional.of(stock));
		
		PurchaseResponse response = impl.updateStock(10, 100);
		assertEquals(PurchaseResponse.LESS_QUANTITY, response);
	}
	
	@Test
	public void testUpdateStockById_List_Higher() {
		Stocks stock = new Stocks();
		stock.setMid(10);
		stock.setMaterialName("Iron");
		stock.setQuantity(200);

		when(stockRepository.existsById(10)).thenReturn(true);
		when(stockRepository.findById(10)).thenReturn(Optional.of(stock));
		
		PurchaseResponse response = impl.updateStock(10, 20);
		assertEquals(PurchaseResponse.OK, response);
	}
	
	@Test
	public void testUpdateStockByName() {
		PurchaseResponse response = impl.updateStock("Plastic", 200);
		assertEquals(PurchaseResponse.STOCK_NOTFOUND, response);
	}
	
	@Test
	public void testUpdateStockByName_List() {
		List<Stocks> list = new ArrayList<Stocks>();
		Stocks stock = new Stocks();
		stock.setMid(1);
		stock.setMaterialName("Plastic");
		stock.setQuantity(20);
		list.add(stock);

		when(stockRepository.findAll()).thenReturn(list);
		
		PurchaseResponse response = impl.updateStock("Plastic", 200);
		assertEquals(PurchaseResponse.LESS_QUANTITY, response);
	}
	
	@Test
	public void testUpdateStockByName_List_Higher() {
		List<Stocks> list = new ArrayList<Stocks>();
		Stocks stock = new Stocks();
		stock.setMid(1);
		stock.setMaterialName("Plastic");
		stock.setQuantity(200);
		list.add(stock);

		when(stockRepository.findAll()).thenReturn(list);
		
		PurchaseResponse response = impl.updateStock("Plastic", 20);
		assertEquals(PurchaseResponse.OK, response);
	}
	
	@Test
	public void testAddStock() {
		List<Stocks> list = new ArrayList<Stocks>();
		Stocks stock = new Stocks();
		stock.setMid(1);
		stock.setMaterialName("Plastic");
		list.add(stock);

		when(stockRepository.findAll()).thenReturn(list);
		impl.addStock("Plastic", 100);
		Assert.assertTrue(true);
	}
	
	@Test
	public void testAddStock_emptyList() {
		impl.addStock("Plastic", 100);
		Assert.assertTrue(true);
	}

}
