package com.vidhya.springdata.ims.domain;

import java.util.List;

public interface StocksService {
	List<Stocks> getStocks();
	Stocks getStockById(int id);
	PurchaseResponse updateStock(int stockid,int quantity);
	void addStock(String stockName,int quantity);
	PurchaseResponse updateStock(String stockName, int quantity);
	Stocks getStock(String stockName);
	boolean deleteStock(String stockName);
}
