package com.vidhya.springdata.ims.domain;

public enum PurchaseResponse {
	LESS_QUANTITY, OK, STOCK_NOTFOUND,UPDATE_SUPPLIER,CREATE_SUPPLIER;
}
