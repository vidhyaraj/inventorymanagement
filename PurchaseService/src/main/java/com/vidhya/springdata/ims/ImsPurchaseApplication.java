package com.vidhya.springdata.ims;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
//@EnableEurekaClient
@SpringBootApplication
public class ImsPurchaseApplication {

	public static void main(String[] args) {
		SpringApplication.run(ImsPurchaseApplication.class, args);
	}

}
