package com.vidhya.springdata.ims.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="materials")
public class Stocks {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int mid;
	
	@Column(name = "quantity")
	private int quantity;
	
	@Column(name = "mtype")
	private String type;
	
	@Column(name = "mname")
	private String materialName;
	
	@ManyToOne
    @JoinColumn(name="supplier_id")
    private Supplier supplier;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getMid() {
		return mid;
	}

	public void setMid(int mid) {
		this.mid = mid;
	}

	
	public Supplier getSplr() {
		return supplier;
	}

	public void setSplr(Supplier splr) {
		this.supplier = splr;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getMaterialName() {
		return materialName;
	}

	public void setMaterialName(String materialName) {
		this.materialName = materialName;
	}
}
