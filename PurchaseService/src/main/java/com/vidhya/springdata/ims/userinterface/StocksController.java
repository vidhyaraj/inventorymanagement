package com.vidhya.springdata.ims.userinterface;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.vidhya.springdata.ims.domain.PurchaseResponse;
import com.vidhya.springdata.ims.domain.Stocks;
import com.vidhya.springdata.ims.domain.StocksService;

@RestController
@RequestMapping("/api")
public class StocksController {

	@Autowired
	private StocksService stockService;

	@GetMapping(value = "/stocks")
	public List<Stocks> getRawMaterials() {
		return stockService.getStocks();
	}

	@RequestMapping(value = "/stocks/{id}")
	public Stocks getMaterial(@PathVariable("id") int id) {
        return stockService.getStockById(id);
	}
	
	@GetMapping(value = "/stocks/name")
	public Stocks getMaterial(@RequestParam("stockName") String stockName) {
		return stockService.getStock(stockName);
	}


	@PutMapping(value = "/stocks/update")
	public ResponseEntity<String> updateMaterial(@RequestParam int stockid,@RequestParam int quantity) {
		PurchaseResponse value =   stockService.updateStock(stockid, quantity);
	    if(value == PurchaseResponse.LESS_QUANTITY) {
			return ResponseEntity.status(HttpStatus.OK).body("Failed Stock quantity is less than requested  " + stockid);
		} else if (value == PurchaseResponse.OK) {
			return ResponseEntity.status(HttpStatus.OK).body("Updated Stock quantity for  " + stockid);
		} else {
	     return ResponseEntity.status(HttpStatus.OK).body("Failed Unable to find stock  " + stockid);
		}
	}
	
	@PutMapping(value = "/stocks/updatebyname")
	public ResponseEntity<String> updateMaterial(@RequestParam String stockName,@RequestParam int quantity) {
		PurchaseResponse value =   stockService.updateStock(stockName, quantity);
	    if(value == PurchaseResponse.LESS_QUANTITY) {
			return ResponseEntity.status(HttpStatus.OK).body("Failed Stock quantity is less than requested  " + stockName);
		} else if (value == PurchaseResponse.OK) {
			return ResponseEntity.status(HttpStatus.OK).body("Updated Stock quantity for  " + stockName);
		} else {
	     return ResponseEntity.status(HttpStatus.OK).body("Failed Unable to find stock  " + stockName);
		}
	}

	@PutMapping(value = "/stocks/add")
	public ResponseEntity<String> addMaterial(@RequestParam String stockName,@RequestParam int quantity) {
       stockService.addStock(stockName, quantity);
       return ResponseEntity.status(HttpStatus.OK).body("Updated stock quantity  " + stockName);
	}
	
	@DeleteMapping(value = "/stocks/delete")
	public ResponseEntity<String> deleteStock(@RequestParam String stockName) {
		if( stockService.deleteStock(stockName))
			return ResponseEntity.status(HttpStatus.OK).body("Stock is deleted for " + stockName);
		return ResponseEntity.status(HttpStatus.OK).body("Stock not found "+ stockName);
	}

}
