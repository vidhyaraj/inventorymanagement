package com.vidhya.springdata.ims.domain;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vidhya.springdata.ims.infrastructure.SupplierRepository;

@Service
public class PurchaseServiceImpl implements PurchaseService {
	
	private static Logger logger = LogManager.getLogger(PurchaseServiceImpl.class);
	private SupplierRepository repository;

	@Autowired
	public PurchaseServiceImpl(SupplierRepository reposiroty) {
		this.repository = reposiroty;
	}

	/**
	 * @param supplierName
	 * @return Supplier
	 */
	Supplier getSupplierByName(String supplierName) {
		List<Supplier> lst = repository.findAll();
		/* Iterate list and get the supplier object by name*/
		for (Supplier obj : lst) {
			if (obj.getSupplierName() != null && supplierName.equalsIgnoreCase(obj.getSupplierName())) {
				return obj;
			}
		}
		return null;
	}

	/**
	 * @param supplierName
	 * @param quant
	 * @param stockType
	 * @param stockname
	 * @return void
	 */
	@Override
	public void createSupplier(String supplierName, int quant, String stockType, String stockname) {
		logger.info("SupplierName : {}",supplierName);
		logger.info("Quantity : {}",quant);
		Stocks stockobj = new Stocks();
		stockobj.setQuantity(quant);
		stockobj.setType(stockType);
		stockobj.setMaterialName(stockname);
		Supplier supobj = new Supplier();
		supobj.setSupplierName(supplierName);
		supobj.addMaterials(stockobj);
		repository.save(supobj);
	}

	/**
	 * @param supplierName
	 * @param quant
	 * @param stockType
	 * @param stockname
	 * @return PurchaseResponse
	 */
	@Override
	public PurchaseResponse updateSupplier(String supplierName, int quant, String stockType, String stockname) {
		boolean isUpdated = false;
		logger.info("SupplierName : {}",supplierName);
		logger.info("Quantity : {}",quant);
		logger.info("Type : {}",stockType);
		Supplier supobj = getSupplierByName(supplierName);
		if (supobj != null) {
			Set<Stocks> smat = supobj.getMaterials();
			for (Stocks obj : smat) {
				if (obj.getMaterialName() != null && stockname.equals(obj.getMaterialName())) {
					obj.setQuantity(obj.getQuantity() + quant);
					obj.setType(stockType);
					obj.setMaterialName(stockname);
					isUpdated = true;
					break;
				}
			}
			if (!isUpdated) {
				Stocks stockobj = new Stocks();
				stockobj.setQuantity(quant);
				stockobj.setType(stockType);
				stockobj.setMaterialName(stockname);
				supobj.addMaterials(stockobj);
			}
			repository.save(supobj);
			logger.info("Update Supplier ");
			return PurchaseResponse.UPDATE_SUPPLIER;
		} else {
			Stocks stockobj = new Stocks();
			stockobj.setQuantity(quant);
			stockobj.setType(stockType);
			stockobj.setMaterialName(stockname);
			supobj = new Supplier();
			supobj.addMaterials(stockobj);
			supobj.setSupplierName(supplierName);
			logger.info("Create Supplier ");
			repository.save(supobj);
			return PurchaseResponse.CREATE_SUPPLIER;
		}
	}

	/**
	 *@return list of supplier
	 */
	@Override
	public List<Supplier> getSupplier() {
		return repository.findAll();
	}

	/**
	 *@return Supplier
	 */
	@Override
	public Supplier getSupplier(int id) {
		if (repository.existsById(id)) {
			Optional<Supplier> value = repository.findById(id);
			if(value.isPresent()) return value.get();
		}
		return null;
	}

	/**
	 * @param supplierName
	 * @return boolean
	 */
	public boolean deleteSupplier(String supplierName) {
		List<Supplier> supplierList = getSupplier();
		/* Iterate list and get the supplier object to delete*/
		for (Supplier supplier : supplierList) {
			if (supplier.getSupplierName().equals(supplierName)) {
				repository.delete(supplier);
				return true;
			}
		}
		return false;
	}
}
