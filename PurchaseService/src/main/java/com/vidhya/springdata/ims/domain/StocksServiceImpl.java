package com.vidhya.springdata.ims.domain;

import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vidhya.springdata.ims.infrastructure.StocksRepository;

@Service
public class StocksServiceImpl implements StocksService {
	private StocksRepository repository;
	private static Logger logger = LogManager.getLogger(StocksServiceImpl.class);

	@Autowired
	public StocksServiceImpl(StocksRepository reposiroty) {
		this.repository = reposiroty;
	}
	
	/**
	 * @param stockName
	 * @return Stocks
	 */
	public Stocks getStockByName(String stockName) {
		List<Stocks> list = repository.findAll();
		for (Stocks obj : list) {
			if (obj.getMaterialName() != null && obj.getMaterialName().equals(stockName)) {
				return obj;
			}
		}
		return null;
	}

	/**
	 * @param stockId
	 * @param quantity
	 * @return PurchaseResponse
	 */
	public PurchaseResponse updateStock(int stockid, int quantity) {
		logger.info("Stockid {}", stockid);
		logger.info("Quantity : {}", quantity);
		if (repository.existsById(stockid)) {
			Optional<Stocks> value = repository.findById(stockid);
			/* Check stock is present or not */
			if (value.isPresent()) {
				Stocks obj = value.get();
				int currentQuantity = obj.getQuantity();
				/* Updating stock quantity by reducing */
				if (currentQuantity < quantity) {
					return PurchaseResponse.LESS_QUANTITY;
				} else {
					obj.setQuantity(currentQuantity - quantity);
					repository.save(obj);
					return PurchaseResponse.OK;
				}
			}
		}
		return PurchaseResponse.STOCK_NOTFOUND;
	}

	/**
	 * @param stockName
	 * @param quantity
	 * @return PurchaseResponse
	 */
	public PurchaseResponse updateStock(String stockName, int quantity) {
		List<Stocks> list = repository.findAll();
		for (Stocks obj : list) {
			if (obj.getMaterialName() != null && obj.getMaterialName().equals(stockName)) {
				int currentQuantity = obj.getQuantity();
				/* Updating stock quantity by reducing */
				if (currentQuantity < quantity) {
					return PurchaseResponse.LESS_QUANTITY;
				} else {
					obj.setQuantity(currentQuantity - quantity);
					repository.save(obj);
					return PurchaseResponse.OK;
				}
			}
		}
		return PurchaseResponse.STOCK_NOTFOUND;
	}

	/**
	 * @param stockName
	 * @param quantity
	 * @return void
	 */
	public void addStock(String stockName, int quantity) {
		boolean newEntry = true;
		List<Stocks> list = repository.findAll();
		Stocks stockObj = null;
		for (Stocks obj : list) {
			/* updating existing stock */
			if (obj.getMaterialName() != null && obj.getMaterialName().equals(stockName)) {
				int tempQuantity = obj.getQuantity();
				obj.setQuantity(tempQuantity + quantity);
				stockObj = obj;
				newEntry = false;
				break;
			}
		}

		/* Adding new stock to repo */
		if (newEntry) {
			stockObj = new Stocks();
			stockObj.setMaterialName(stockName);
			stockObj.setQuantity(quantity);
		}

		repository.save(stockObj);
	}

	/**
	 * @return List<Stocks>
	 */
	@Override
	public List<Stocks> getStocks() {
		return repository.findAll();
	}

	/**
	 * @param id
	 * @return Stocks
	 */
	@Override
	public Stocks getStockById(int id) {
		if (repository.existsById(id)) {
			Optional<Stocks> value = repository.findById(id);
			if(value.isPresent()) return value.get();
		}
		return null;
	}

	/**
	 * @param stockName
	 * @return boolean
	 */
	@Override
	public boolean deleteStock(String stockName) {
		List<Stocks> stockList = getStocks();
		for (Stocks stock : stockList) {
			if (stock.getMaterialName() != null && stock.getMaterialName().equals(stockName)) {
				repository.delete(stock);
				return true;
			}
		}
		return false;
	}

	/**
	 * @param stockName
	 * @return Stocks
	 */
	@Override
	public Stocks getStock(String stockName) {
		return getStockByName(stockName);
	}

}
