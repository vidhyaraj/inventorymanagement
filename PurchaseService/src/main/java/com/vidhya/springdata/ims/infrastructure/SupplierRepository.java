package com.vidhya.springdata.ims.infrastructure;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vidhya.springdata.ims.domain.Supplier;

public interface SupplierRepository extends JpaRepository<Supplier, Integer> {

}
