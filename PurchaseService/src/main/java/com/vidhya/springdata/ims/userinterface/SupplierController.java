package com.vidhya.springdata.ims.userinterface;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.vidhya.springdata.ims.domain.PurchaseResponse;
import com.vidhya.springdata.ims.domain.PurchaseService;
import com.vidhya.springdata.ims.domain.Supplier;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class SupplierController {

	@Autowired
	private PurchaseService purchaseService;

	@Cacheable("ims")
	@GetMapping(value = "/suppliers")
	public List<Supplier> getSuppliers() {
		return purchaseService.getSupplier();
	}

	@GetMapping(value = "/supplierss")
	public ResponseEntity<String> getSupplierss() {
		List<Supplier> obj = purchaseService.getSupplier();
		if(!obj.isEmpty()) {
			for(Supplier temp : obj) {
				if(temp.getSupplierName() != null) {
					return ResponseEntity.status(HttpStatus.OK).body("Supplier name is  " + temp.getSupplierName());
				}
			}
		}
		return ResponseEntity.status(HttpStatus.OK).body("Supplier not found in list");
		
	}

	@RequestMapping(value = "/suppliers/{id}")
	public Supplier getSupplier(@PathVariable("id") int id) {
		return purchaseService.getSupplier(id);
	}
	
	@CacheEvict(value="ims", allEntries=true)
	@PostMapping(value = "/supplier/create")
	public ResponseEntity<String> createSupplier(@RequestParam(value = "sname") String supplierName,
			@RequestParam(value = "quant") String quant, @RequestParam(value = "type") String stockType,
			@RequestParam(value = "stockname") String stockname) {
		purchaseService.createSupplier(supplierName, Integer.parseInt(quant), stockType, stockname);
		return ResponseEntity.status(HttpStatus.OK).body("Created entry for  " + supplierName);
	}
	
	@CacheEvict(value="ims", allEntries=true)
	@PostMapping(value = "/supplier/update")
	public ResponseEntity<String> updateSupplier(@RequestParam(value = "sname") String supplierName,
			@RequestParam(value = "quant") String quant, @RequestParam(value = "type") String stockType,
			@RequestParam(value = "stockname") String stockname) {
		PurchaseResponse value = purchaseService.updateSupplier(supplierName, Integer.parseInt(quant), stockType, stockname);
		if (value == PurchaseResponse.UPDATE_SUPPLIER) {
			return ResponseEntity.status(HttpStatus.OK).body("Updated supplier entry for  " + supplierName);
		} else {
			return ResponseEntity.status(HttpStatus.OK).body("Create supplier entry for  " + supplierName);
		}
	}
	
	@CacheEvict(value="ims", allEntries=true)
	@DeleteMapping(value = "/supplier/delete")
	public ResponseEntity<String> deleteStock(@RequestParam String supplierName) {
		if (purchaseService.deleteSupplier(supplierName))
			return ResponseEntity.status(HttpStatus.OK).body("Supplier is deleted for " + supplierName);
		return ResponseEntity.status(HttpStatus.OK).body("Stock not found " + supplierName);
	}
	
	@CacheEvict(value="ims", allEntries=true)
	@DeleteMapping(value = "/supplier/deletes")
	public ResponseEntity<String> deleteStocks(@RequestBody String supplierName) {
		if (purchaseService.deleteSupplier(supplierName))
			return ResponseEntity.status(HttpStatus.OK).body("Supplier is deleted for " + supplierName);
		return ResponseEntity.status(HttpStatus.OK).body("Stock not found " + supplierName);
	}
}
