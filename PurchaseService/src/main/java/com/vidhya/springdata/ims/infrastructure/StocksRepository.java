package com.vidhya.springdata.ims.infrastructure;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vidhya.springdata.ims.domain.Stocks;

public interface StocksRepository extends JpaRepository<Stocks, Integer>{

}
