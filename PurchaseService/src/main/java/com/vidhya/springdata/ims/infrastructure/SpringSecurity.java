package com.vidhya.springdata.ims.infrastructure;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class SpringSecurity extends WebSecurityConfigurerAdapter{

	// Authentication : User --> Roles
	    @Override
		protected void configure(AuthenticationManagerBuilder auth)
				throws Exception {
			auth.inMemoryAuthentication()
			.withUser("user").password("{noop}password").roles("USER")
            .and()
            .withUser("admin").password("{noop}password").roles("USER", "ADMIN");
		}

		// Authorization : Role -> Access
	    @Override
		protected void configure(HttpSecurity http) throws Exception {
			http.httpBasic().and()
			.authorizeRequests()
			.antMatchers(HttpMethod.GET, "/api/test/**").hasRole("USER")
			.and().csrf().disable().formLogin().disable();
		}
}
