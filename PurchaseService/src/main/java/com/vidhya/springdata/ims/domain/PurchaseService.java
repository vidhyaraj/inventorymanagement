package com.vidhya.springdata.ims.domain;

import java.util.List;

public interface PurchaseService {
	List<Supplier> getSupplier();
	Supplier getSupplier(int id);
	void createSupplier(String supplierName, int quant, String stockType, String stockname);
	boolean deleteSupplier(String supplierName);
	PurchaseResponse updateSupplier(String supplierName, int quant, String stockType, String stockname);
}
